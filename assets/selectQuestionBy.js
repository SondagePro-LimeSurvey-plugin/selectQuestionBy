/**
 * @file Description
 * @author Denis Chenu
 * @copyright Denis Chenu <http://www.sondages.pro>
 * @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt Expat (MIT)
 */

$(function() {
  $("[data-selectQuestionBy='simple']").select2({
    theme: "bootstrap",
    //~ dropdownParent: $('.answer-item')
  });
  $("[data-selectQuestionBy='multiple']").select2({
    theme: "bootstrap",
    //~ dropdownParent: $('.answer-item'),
    escapeMarkup: function (markup) { return markup; },
    templateSelection:function (data) {
      if(data.element && $(data.element).data("group") ) {
        return "<strong>["+$(data.element).data("group")+"]</strong> "+data.text;
      }
      return data.text
    },
    templateResult:function (data) {
      return data.text
    }
  });
  $("[data-selectQuestionBy='multiple']").on("change", function (e) {
    if($(this).val()){
      $("#"+$(this).data('update')).val($(this).val().join(',')).trigger("keyup");
    } else {
      $("#"+$(this).data('update')).val("").trigger("keyup");
    }
  });
});
$(document).on("select2:unselecting","[data-selectQuestionBy='multiple'][data-forceone=1]",function(e){
    if($(this).val().length < 2) {
        e.preventDefault();
        return false;
    }
});
